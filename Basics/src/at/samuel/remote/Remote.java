package at.samuel.remote;

import java.util.ArrayList;

public class Remote {
	private double weight;
	private double length;
	private double width;
	private double heigth;
	private boolean isOn = false;
	
	private ArrayList<Battery> batteries;

	public Remote(double w, double l, double wi, double h) {
		this.weight = w;
		this.length = l;
		this.width = wi;
		this.heigth = h;
		this.batteries = new ArrayList<>();
	}

	public void turnOn() {
		System.out.println("I am turned on now");
		this.isOn = true;
	}

	public void turnOff() {
		System.out.println("I am turned on now");
		this.isOn = false;
	}

	public void sayHello() {
		System.out.println("Weight: " + this.weight);
		System.out.println(this.isOn);
	}

	

}
