package at.samuel.car;

import java.util.ArrayList;

public class Person {
	private String firstname;
	private String lastname;
	private String birthdate;

	private ArrayList<Car> cars;

	public Person(String firstname, String lastname, String birthdate) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthdate = birthdate;
		this.cars = new ArrayList<>();
	}
	
	public String carInfo() {
		String carColor = "";
		for (Car c : cars) {
			carColor = carColor + c.getColor() + "\n";
		}
		return carColor;
	}
	
	//int getValueOfCars() {
	//	for (Car c: cars) {
			
	//	}
	//}
	
	public void personalData() {
		System.out.println("\nFirstname: " + this.firstname + "\nLastname: " + this.lastname + "\nBirthdate: " + this.birthdate + "\nCars: " + this.carInfo());
	}

	public void addCar(Car c) {
		this.cars.add(c);
	}

}
