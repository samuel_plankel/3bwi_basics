package at.samuel.car;

public class CarStarter {

	public static void main(String[] args) {
		Producer p1 = new Producer ("Nissan", "Japan", 0.9);
		Producer p2 = new Producer ("Mustang", "USA", 0.8);
		Engine e1 = new Engine("gas", 600);
		Engine e2 = new Engine("gas", 500);
		Car c1 = new Car("Blue", 340, 80000, 4, e1, p1);
		Car c2 = new Car("Black", 500, 60000, 2, e2, p2);
		Person pers1 = new Person ("Franz", "Hinterseher", "31.12.1999");
		
		pers1.addCar(c1);
		pers1.addCar(c2);
		
		pers1.personalData();
		c1.getPrice();
	}

}
