package at.samuel.car;

public class Engine {
	private String fuelType;
	private int horsePower;
	
	public Engine(String fuelType, int horsePower) {
		this.fuelType = fuelType;
		this.horsePower = horsePower;
	}
}
