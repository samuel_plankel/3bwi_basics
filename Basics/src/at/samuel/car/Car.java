package at.samuel.car;

public class Car {
	private String color;
	private int maxVelocity;
	private double basePrice;
	private int baseGasConsume;
	private Engine engine;
	private Producer producer;
	private double price;

	public Car(String color, int maxVelocity, double basePrice, int baseGasConsume, Engine engine, Producer producer) {
		this.color = color;
		this.maxVelocity = maxVelocity;
		this.basePrice = basePrice;
		this.baseGasConsume = baseGasConsume;
		this.engine = engine;
		this.producer = producer;
		double price = this.basePrice * producer.getSale();
		this.price = price;
	}

	public double getPrice() {
		System.out.println(this.price);
		return price;
	}

	public String getColor() {
		return color;
	}

	public int getMaxVelocity() {
		return maxVelocity;
	}

	public double getBasePrice() {
		return basePrice;
	}

	public int getBaseGasConsume() {
		return baseGasConsume;
	}

	public Engine getEngine() {
		return engine;
	}

	public Producer getProducer() {
		return producer;
	}
	
}
