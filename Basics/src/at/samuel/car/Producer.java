package at.samuel.car;

public class Producer {
	private String name;
	private String country;
	private double sale;

	public Producer(String name, String country, double sale) {
		this.name = name;
		this.country = country;
		this.sale = sale;
	}

	public double getSale() {
		return sale;
	}
	
	
	
}
