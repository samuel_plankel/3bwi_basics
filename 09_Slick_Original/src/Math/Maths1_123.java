package Math;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import towerDefence.Game;




public class Maths1_123 extends BasicGame{
	private double rente = 1200;
	private double barwert = 10000;
	private double endwert;
	private double endwert2;
	private double n;
	private double quotient = 1.0125;
	private double gesamtEndwert;
	private String output = "Endwert 1 = " + endwert;
	
	
	public Maths1_123() {
		super("Maths1_123");
		// TODO Auto-generated constructor stub
	}
	
	

	public Maths1_123(String title, double rente, double barwert, double endwert, double endwert2, int n, double quotient) {
		super(title);
		this.rente = rente;
		this.barwert = barwert;
		this.endwert = endwert;
		this.endwert = endwert2;
		this.n = n;
		this.quotient = quotient;
	}



	public void nachnJahre() {
		this.n = 45;
		
		this.endwert = this.barwert * Math.pow(this.quotient, this.n);
		System.out.println("Endwert 1: " + this.endwert);
	}
	
	public void einzahlungnJahre() {
		this.n = 35;
		
		this.endwert2 = this.rente * (1 - Math.pow(this.quotient, this.n)) / (1 - this.quotient) * this.quotient;
		System.out.println("Endwert 2: " + this.endwert2);
	}
	
	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		g.drawString(this.output, 50, 50);
	}

	public void nRente() {
		this.rente = 2000;
		this.gesamtEndwert = this.endwert + this.endwert2;
		
		this.n = Math.log(- this.rente / ((this.gesamtEndwert * this.quotient) - this.gesamtEndwert - this.rente)) / Math.log(this.quotient);
		System.out.println("n: " + this.n);
	}
	
	@Override
	public void init(GameContainer arg0) throws SlickException {
		nachnJahre();
		einzahlungnJahre();
		nRente();
		this.gesamtEndwert = this.endwert + this.endwert2;
		System.out.println(this.gesamtEndwert); 
	}

	@Override
	public void update(GameContainer arg0, int arg1) throws SlickException {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
		try {
			AppGameContainer container = new AppGameContainer(new Maths1_123());
			container.setDisplayMode(400, 400, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
