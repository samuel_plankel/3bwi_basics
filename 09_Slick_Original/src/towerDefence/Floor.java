package towerDefence;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Point;

import at.samuel.landscape.Actor;

public class Floor implements Actor {
	private int x,y;
	private Image floor; 

	public Floor(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		
		try {
			floor = new Image("images/floor.png");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void render(Graphics graphics) {
		Point p = CoordinateHelper.getPointForWall(x, y);
		floor.draw(p.getX(), p.getY(), Game.GRID_SIZE, Game.GRID_SIZE);	
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	
	
	

}
