package towerDefence;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Point;

import at.samuel.landscape.Actor;

public class SimpleTower implements Actor {
	private int x,y;
	private Game game;
	private Image tower;
	

	public SimpleTower(int x, int y, Game game) {
		super();
		this.x = x;
		this.y = y;
		this.game = game;
		
		try {
			tower = new Image("images/tower.png");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void render(Graphics graphics) {
		graphics.setColor(Color.blue);
		Point p = CoordinateHelper.getPointForSimpleTower(this);
//		graphics.fillRect(p.getX(), p.getY(), Game.GRID_SIZE, Game.GRID_SIZE);	
		tower.draw(p.getX(), p.getY(), Game.GRID_SIZE, Game.GRID_SIZE);	
		graphics.drawLine(p.getX() + 26, p.getY() + 26, Game.enemy1.getX() * 52 + 21, Game.enemy1.getY() * 52 + 21);
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	

}
