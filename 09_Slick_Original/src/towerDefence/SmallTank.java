package towerDefence;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Point;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

import at.samuel.landscape.Actor;

public class SmallTank implements Actor {
	private int x, y;
	private int tick;
	private int speed;
	private boolean wallTopRight = false;
	private boolean wallBottomRight = false;
	public Point pb = CoordinateHelper.getPointForSmallTank(this);
	private Shape shape;
	private List<Shape> collissionPartner;
	public static boolean bulletHit = false;
	private int health = 100;
	private final int DAMAGE = 10;

	public SmallTank() {
		this.collissionPartner = new ArrayList<>();
		this.x = 0;
		this.y = 3;
		this.tick = 0;
		this.speed = 1000;
		this.shape = new Rectangle((float)this.x, (float)this.y, Game.GRID_SIZE, Game.GRID_SIZE);
		
		
	}

	@Override
	public void render(Graphics graphics) {
		graphics.setColor(Color.pink);
		Point p = CoordinateHelper.getPointForWall(this.x, this.y);
		graphics.fillOval(p.getX(), p.getY(), Game.GRID_SIZE, Game.GRID_SIZE);
		graphics.setColor(Color.green);
		graphics.draw(this.shape);

	}

	@Override
	public void update(GameContainer gc, int delta) {
		this.tick += delta;
		
		if (this.health == 0) {
			this.x = 0;
			this.y = 3;
			int i = 10;
			this.health = 100 + i;
			i = i + 10;
		}

		if (Game.multi[this.x][this.y] == 0) {
			if (Game.multi[this.x + 1][this.y] == 0) {
				if (tick > speed) {
					this.x++;
					this.tick = 0;
				}
			}

			if (this.wallTopRight == true) {
				if (tick > speed && this.wallTopRight == true) {
					if (Game.multi[this.x][this.y + 1] == 1) {
						this.wallTopRight = false;
					}

					else {
						this.y++;
						this.tick = 0;
					}
				}
			}

			if (Game.multi[this.x][this.y - 1] == 1) {
				this.wallTopRight = true;
			}

			else if (Game.multi[this.x][this.y + 1] == 1) {
				this.wallTopRight = false;

			}

			if (this.wallBottomRight == true) {
				if (tick > speed && this.wallBottomRight == true) {
					if (Game.multi[this.x][this.y - 1] == 1) {
						this.wallBottomRight = false;
					}

					else {
						this.y--;
						this.tick = 0;
					}

				}
			}

			if (Game.multi[this.x + 1][this.y + 1] == 1) {
				this.wallBottomRight = true;

			}
		}
		
		for (Shape s : collissionPartner) {
			if (s.intersects(this.shape) && bulletHit == false) {
				System.out.println("Bum Bum Unfall!!!");
				this.health = this.health - DAMAGE;
				System.out.println(this.health);
				bulletHit = true;
			}
		}
		
		shape.setX((float) this.x * Game.GRID_SIZE);
		shape.setY((float) this.y * Game.GRID_SIZE);
	}
	
	public void addPartner(Shape shape) {
		this.collissionPartner.add(shape);
	}
	
	public Shape getShape() {
		return shape;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	
}
