package towerDefence;

import java.util.ArrayList;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.gui.AbstractComponent;
import org.newdawn.slick.gui.MouseOverArea;

import at.samuel.landscape.Actor;
import at.samuel.landscape.Player;


public class Game extends BasicGame {
	private static final double SPEED = 0.3;
	public static final int GRID_SIZE = 52;
	private Image floor;
	

	private ArrayList<Actor> actors;

	public static int[][] multi = new int[][] { { 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 1, 2, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
												{ 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, };

	private MouseOverArea[] shopAreas = new MouseOverArea[4];
	private Image image;
	private String message = "To let the Tower shoot, \npress Space ONCE!";
	public static SmallTank enemy1;
	

	public Game() {
		super("Game");
	}

	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		for (Actor actor : this.actors) {
			actor.render(graphics);
		}
		
		for (int i=0;i<4;i++) {
			shopAreas[i].render(gc, graphics);
		}
		graphics.setColor(Color.black);
		graphics.drawString(message, 600, 50);
		
		
	}

	public void init(GameContainer gc) throws SlickException {
		this.actors = new ArrayList<>();
		
		for (int x = 0; x < multi.length; x++) {
			for (int y = 0; y < multi.length; y++) {
				if (multi[x][y] == 0) {
					this.actors.add(new Floor(x, y));
				}
			}
		}
		
		for (int x = 0; x < multi.length; x++) {
			for (int y = 0; y < multi.length; y++) {
				if (multi[x][y] == 1) {
					this.actors.add(new Wall(x, y, this));
				}
			}
		}
		
		for (int x = 0; x < multi.length; x++) {
			for (int y = 0; y < multi.length; y++) {
				if (multi[x][y] == 2) {
					this.actors.add(new SimpleTower(x, y, this));
				}
			}
		}
		this.enemy1 = new SmallTank();
		this.actors.add(enemy1);
		
		
		image = new Image("images/shopArea1.png");
		
		for (int i=0;i<4;i++) {
			shopAreas[i] = new MouseOverArea(gc, image, 20 * GRID_SIZE,  (i*200), 399, 200);
			shopAreas[i].setNormalColor(new Color(1,1,1,0.8f));
			shopAreas[i].setMouseOverColor(new Color(1,1,1,0.9f));
		}
		
		
	}

	public void update(GameContainer gc, int delta) throws SlickException {
		for (Actor actor : this.actors) {
			actor.update(gc, delta);
		}
	}
	
	public void componentActivated(AbstractComponent source) {
		System.out.println("");
		System.out.println("ACTIVL : "+source);
		for (int i=0;i<4;i++) {
			if (source == shopAreas[i]) {
				message = "Option "+(i+1)+" pressed!";
			}
		}
	}

	public void keyPressed(int key, char c) {
	
		super.keyPressed(key, c);
		if (key==Input.KEY_SPACE) {
			for (int x = 0; x < multi.length; x++) {
				for (int y = 0; y < multi.length; y++) {
					if (multi[x][y] == 2) {
						Bullet b = new Bullet(x,y);
						this.actors.add(b);
						enemy1.addPartner(b.getShape());
					}
				}
			}			
		}
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Game());
			container.setDisplayMode(20 * GRID_SIZE + 400, 16 * GRID_SIZE, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}