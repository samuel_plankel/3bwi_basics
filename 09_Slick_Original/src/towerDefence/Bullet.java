package towerDefence;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Point;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

import at.samuel.landscape.Actor;

public class Bullet implements Actor{
	private double x;
	private double y;
	private int width;
	private int height;
	private Shape shape;
	private int tick;
	private int speed;
	private Image bullet;
	
	public Bullet(int x, int y) {
		super();
		this.x = x + 0.5;
		this.y = y + 0.5;
		this.width = 10;
		this.height = 10;
		this.tick = 0;
		this.speed = 1000;
		this.shape = new Rectangle((float)this.x, (float)this.y, this.width, this.height);
		
		try {
			bullet = new Image("images/bullet.png");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void render(Graphics graphics) {
		graphics.setColor(Color.orange);
		Point p = CoordinateHelper.getPointForBullet(this);
		bullet.draw(p.getX(), p.getY(), this.width, this.height);
//		graphics.fillOval(this.x, this.y, this.width, this.height);		
		graphics.setColor(Color.transparent);
		graphics.draw(shape);
	}

	@Override
	public void update(GameContainer gc, int delta) {
//		this.tick += delta;
		
//		if (tick > speed) {
		if (this.x > Game.enemy1.getX()) {
			this.x = x - 0.01;
		}
		
		if (this.x < Game.enemy1.getX()) {
			this.x = x + 0.01;
		}
		
		if (this.y > Game.enemy1.getY()) {
			this.y = y - 0.01;
		}
		
		if (this.y < Game.enemy1.getY()) {
			this.y = y + 0.01;
		}
//		this.tick = 0;
//		}
		
		Point p = CoordinateHelper.getPointForBullet(this);
		shape.setX((float) p.getX());
		shape.setY((float) p.getY());
		
		if (SmallTank.bulletHit == true) {
			for (int a = 0; a < Game.multi.length; a++) {
				for (int b = 0; b < Game.multi.length; b++) {
					if (Game.multi[a][b] == 2) {
						this.tick += delta;
						
						if (tick > speed) {
							x = a + 0.9;
							y = b + 0.9;
							SmallTank.bulletHit = false;
							System.out.println(SmallTank.bulletHit);
							this.tick = 0;
						}
					}
				}
			}
		}
	}
	
	public Shape getShape() {
		return this.shape;
	}

	public int getX() {
		return (int)x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return (int)y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	
}
