package towerDefence;

import org.newdawn.slick.geom.Point;


public class CoordinateHelper {
	public static Point getPointForWall(Wall wall) {
		Point p = new Point(wall.getX() * Game.GRID_SIZE, wall.getY() * Game.GRID_SIZE);
		return p;
	}
	
	public static Point getPointForWall(int x, int y) {
		Point p = new Point(x * Game.GRID_SIZE, y * Game.GRID_SIZE);
		return p;
	}
	
	public static Point getPointForSmallTank(int x, int y) {
		Point pb = new Point(x * Game.GRID_SIZE, y * Game.GRID_SIZE);
		return pb;
	}

	public static Point getPointForSimpleTower(SimpleTower tower) {
		Point p = new Point(tower.getX() * Game.GRID_SIZE, tower.getY() * Game.GRID_SIZE);
		return p;
	}
	
	public static Point getPointForBullet(Bullet bullet) {
		Point p = new Point(bullet.getX() * Game.GRID_SIZE, bullet.getY() * Game.GRID_SIZE);
		return p;
	}
	
	public static Point getPointForSmallTank(SmallTank sTank) {
		Point pb = new Point(sTank.getX() * Game.GRID_SIZE, sTank.getY() * Game.GRID_SIZE);
		return pb;
	}
	
}
