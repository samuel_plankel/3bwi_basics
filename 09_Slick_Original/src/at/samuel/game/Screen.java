package at.samuel.game;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.File;


public class Screen extends JPanel implements Runnable {
	private Thread thread = new Thread(this);
	public static Image[] groundTiles = new Image[100];
	public static Image[] airTiles = new Image[100];
	public static int myWidth, myHeight;
	private static boolean isFirst = true;
	public static Room room;
	public static Save save;

	public Screen() {
		thread.start();
	}

	public void define() {
		room = new Room();
		save = new Save();
		
		for(int i=0;i<groundTiles.length;i++) {
			groundTiles[i] = new ImageIcon("images/ground2.png").getImage();
//			groundTiles[i] = createImage(new FilteredImageSource(groundTiles[i].getSource(), new CropImageFilter(0, i * 26, 26, 26)));
		}
		
		for(int i=0;i<airTiles.length;i++) {
			airTiles[i] = new ImageIcon("images/air.png").getImage();
//			airTiles[i] = createImage(new FilteredImageSource(airTiles[i].getSource(), new CropImageFilter(0, i * 26, 26, 26)));
		}
		
		save.loadSave(new File("save/level1.sapl"));
	}

	public void paintComponent(Graphics g) {
		if (isFirst) {
			define();

			isFirst = false;
		}

		g.clearRect(0, 0, getWidth(), getHeight());
		
		room.draw(g); //Drawing the room

	}

	public static int fpsFrame = 0, fps = 1000000;

	public void run() {
		while (true) {
			if (!isFirst) {
				myWidth = getWidth();
				myHeight = getHeight();
				define();
				
				room.physic();
			}
				
				repaint();

			try {
				Thread.sleep(1);
			} catch (Exception e) {
			}
		}
	}
}
