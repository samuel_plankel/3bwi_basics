package at.samuel.game;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Wall implements Actor {
	private int x;
	private int y;
	private int height;
	private int width;
	
	

	public Wall(int x, int y, int height, int width) {
		super();
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
	}

	@Override
	public void render(Graphics graphics) {
		graphics.drawRect(this.x,this.y,this.height,this.width);
	}

	@Override
	public void update(GameContainer gc, int delta) {
		
	}
}
