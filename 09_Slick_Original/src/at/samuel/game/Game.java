package at.samuel.game;

import java.awt.GridLayout;
import java.util.ArrayList;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import at.samuel.game.Actor;
import at.samuel.landscape.Player;

public class Game extends BasicGame{
	private ArrayList<Actor> actors;
	private Wall wall;
	
	public Game() {
		super("Game");
	}

	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		graphics.setBackground(Color.blue); 
		for (Actor a : actors) {
			a.render(graphics);
		}	
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		setLayout(new GridLayout(1,1,0,0));
		this.actors = new ArrayList<>();
		
		
	
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Game());
			container.setDisplayMode(780, 780, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
