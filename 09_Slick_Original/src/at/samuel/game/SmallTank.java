package at.samuel.game;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class SmallTank implements Actor {
	private int x;
	private int y;
	private int width;
	private int heigth;
	
	public SmallTank(int x, int y, int width, int heigth) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.heigth = heigth;
	}

	@Override
	public void render(Graphics graphics) {
		graphics.drawRect(this.x, this.y, this.width, this.heigth);
		
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		
	}
	
	
}
